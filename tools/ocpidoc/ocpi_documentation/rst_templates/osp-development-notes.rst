.. %%NAME-CODE%% OSP Development Notes

.. _%%NAME-CODE%%-osp-development-notes:

.. Below is the substitution string used in this template
   in headings and text as a placeholder for the given OSP
   name, e.g, "Avnet", "Xilinx", "Ettus" "Analog Devices Inc".
   Details on how to substitution strings are given in the
   section "Using Include Files and Substitution Strings to
   Share Common Information" in the OpenCPI Documentation
   Writer Guide.
   
.. |osp_name| replace:: OspName
	   
OpenCPI |osp_name| OSP Development Notes
========================================

.. This is the main RST file for an OpenCPI OSP development notes
   document. This is an optional document that can be used like
   a journal or lab notebook to chronicle the OSP development
   process, for example, to describe staging steps, steps to
   enable OpenCPI control and data planes and cards and slots,
   or to provide any other information collected during OSP
   development that may be useful to future maintainers and
   developers of the platform(s) provided in the OSP.

.. Copy this template file, rename it, and edit the contents
   to your requirements.

.. Below are skeleton sections and subsections.

Section XXX
-----------
Add text here.

Subsection aaa
~~~~~~~~~~~~~~

Add text here.

Subsection bbb
~~~~~~~~~~~~~~

Add text here.

Section YYY
-----------

Add text here.
