.. Application directory index page


Applications
============
Test applications for devices in the platform project. 

.. toctree::
   :maxdepth: 2
      :glob:

      */*-application
